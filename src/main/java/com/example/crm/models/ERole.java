package com.example.crm.models;

public enum ERole {
    ROLE_USER,
    ROLE_ADMIN,
    ROLE_TEACHER,
    ROLE_STUDENT,
    ROLE_MANAGER
}
